﻿using System.Drawing;

namespace EQPreview.Mediator.Messages
{
	sealed class ThumbnailActiveSizeUpdated : NotificationBase<Size>
	{
		public ThumbnailActiveSizeUpdated(Size size)
				: base(size)
		{
		}
	}
}