﻿using System.Threading;
using System.Threading.Tasks;
using EQPreview.Mediator.Messages;
using EQPreview.Presenters;
using MediatR;

namespace EQPreview.Mediator.Handlers.Thumbnails
{
	sealed class ThumbnailActiveSizeUpdatedHandler : INotificationHandler<ThumbnailActiveSizeUpdated>
	{
		private readonly IMainFormPresenter _presenter;

		public ThumbnailActiveSizeUpdatedHandler(MainFormPresenter presenter)
		{
			this._presenter = presenter;
		}

		public Task Handle(ThumbnailActiveSizeUpdated notification, CancellationToken cancellationToken)
		{
			this._presenter.UpdateThumbnailSize(notification.Value);

			return Task.CompletedTask;
		}
	}
}