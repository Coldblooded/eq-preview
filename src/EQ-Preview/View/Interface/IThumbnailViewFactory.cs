﻿using System;
using System.Drawing;

namespace EQPreview.View
{
	public interface IThumbnailViewFactory
	{
		IThumbnailView Create(IntPtr id, string title, Size size);
	}
}