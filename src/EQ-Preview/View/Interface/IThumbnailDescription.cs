﻿namespace EQPreview.View
{
	public interface IThumbnailDescription
	{
		string Title { get; set; }
		bool IsDisabled { get; set; }
	}
}