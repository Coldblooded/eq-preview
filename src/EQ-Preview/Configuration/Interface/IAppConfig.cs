﻿namespace EQPreview.Configuration
{
	/// <summary>
	/// Application configuration
	/// </summary>
	public interface IAppConfig
	{
		string ConfigFileName { get; set; }
	}
}