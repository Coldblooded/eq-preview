﻿namespace EQPreview.Configuration
{
	public interface IConfigurationStorage
	{
		void Load();
		void Save();
	}
}