﻿namespace EQPreview
{
	public interface IPresenter
	{
		void Run();
	}
}