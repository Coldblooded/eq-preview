﻿using System;

namespace EQPreview.Services
{
	public interface IProcessInfo
	{
		IntPtr Handle { get; }
		string Title { get; }
	}
}